package com.example.pushnotification;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
 
public class MainActivity extends Activity {
     
    // UI elements
    EditText txtName; 
    EditText txtEmail;
     
    // Register button
    Button btnRegister;
 
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
         
        //Get Global ServerController Class object (see application tag in AndroidManifest.xml)
        final ServerController aController = (ServerController) getApplicationContext();
        final Controller controller = (Controller) getApplicationContext();
        // Check if Internet Connection present
        if (!controller.isConnectingToInternet()) {
             
            // Internet Connection is not present
        	controller.showAlertDialog(MainActivity.this,
                    "Internet Connection Error",
                    "Please connect to working Internet connection", false);
             
            // stop executing code by return
            return;
        }
 
        // Check if GCM configuration is set
        if (Config.YOUR_SERVER_URL == null
            || Config.GOOGLE_SENDER_ID == null
            || Config.YOUR_SERVER_URL.length() == 0
            || Config.GOOGLE_SENDER_ID.length() == 0) {
             
            // GCM sernder id / server url is missing
        	controller.showAlertDialog(MainActivity.this, "Configuration Error!",
                    "Please set your Server URL and GCM Sender ID", false);
             
            // stop executing code by return
             return;
        }
         
        txtName = (EditText) findViewById(R.id.txtName);
        txtEmail = (EditText) findViewById(R.id.txtEmail);
        btnRegister = (Button) findViewById(R.id.btnRegister);
         
        // Click event on Register button
        btnRegister.setOnClickListener(new View.OnClickListener() {
             
            @Override
            public void onClick(View arg0) {  
                // Get data from EditText 
                String name = txtName.getText().toString(); 
                String email = txtEmail.getText().toString();
                 
                // Check if user filled the form
                if(name.trim().length() > 0 && email.trim().length() > 0){
                     
                    // Launch Main Activity
                    Intent i = new Intent(getApplicationContext(), PushNotifActivity.class);
                     
                    // Registering user on our server                   
                    // Sending registraiton details to MainActivity
                    i.putExtra("name", name);
                    i.putExtra("email", email);
                    startActivity(i);
                    finish();
                     
                }else{
                     
                    // user doesn't fill that data
                	controller.showAlertDialog(MainActivity.this, 
                                                "Registration Error!", 
                                                "Please enter your details", 
                                                false);
                }
            }
        });
    }
 
}