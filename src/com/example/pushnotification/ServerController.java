package com.example.pushnotification;

import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Random;
import java.util.Map.Entry;

import org.apache.http.HttpStatus;
import org.apache.http.protocol.HTTP;
 
import com.google.android.gcm.GCMRegistrar; 
 
import android.app.AlertDialog;
import android.app.Application;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.PowerManager;
import android.util.Log;
 
public class ServerController extends Application{
     
    private  final int MAX_ATTEMPTS = 5;
    private  final int BACKOFF_MILLI_SECONDS = 2000;
    private  final Random random = new Random();
    private   Controller controller = (Controller) getApplicationContext();
     
     // Register this account with the server.
    void register(final Context context, String name, String email, final String regId) {
          
        Log.i(Config.TAG, "registering device (regId = " + regId + ")");
         
        String serverUrl = Config.YOUR_SERVER_URL;
         
        Map<String, String> params = new HashMap<String, String>();
        params.put("regId", regId);
        params.put("name", name);
        params.put("email", email);
         
        long backoff = BACKOFF_MILLI_SECONDS + random.nextInt(1000);
         
        // Once GCM returns a registration id, we need to register on our server
        // As the server might be down, we will retry it a couple
        // times.
        for (int i = 1; i <= MAX_ATTEMPTS; i++) {
             
            Log.d(Config.TAG, "Attempt #" + i + " to register");
             
            try {
                //Send Broadcast to Show message on screen
            	controller.displayMessageOnScreen(context,
                       "Registering on Server "+ i + MAX_ATTEMPTS);
                 
                // Post registration values to web server
                post(serverUrl, params);
                 
                GCMRegistrar.setRegisteredOnServer(context, true);
                 
                //Send Broadcast to Show message on screen
                //String message = context.getString("Register");
                controller.displayMessageOnScreen(context, "Registered on Server");
                 
                return;
            } catch (IOException e) {
                 
                // Here we are simplifying and retrying on any error; in a real
                // application, it should retry only on unrecoverable errors
                // (like HTTP error code 503).
                 
                Log.e(Config.TAG, "Failed to register on attempt " + i + ":" + e);
                 
                if (i == MAX_ATTEMPTS) {
                    break;
                }
                try {
                     
                    Log.d(Config.TAG, "Sleeping for " + backoff + " ms before retry");
                    Thread.sleep(backoff);
                     
                } catch (InterruptedException e1) {
                    // Activity finished before we complete - exit.
                    Log.d(Config.TAG, "Thread interrupted: abort remaining retries!");
                    Thread.currentThread().interrupt();
                    return;
                }
                 
                // increase backoff exponentially
                backoff *= 2;
            }
        }
         
//        String message = context.getString(R.string.server_register_error,
//                MAX_ATTEMPTS);
         
        //Send Broadcast to Show message on screen
        controller.displayMessageOnScreen(context, "Registeration Error"+MAX_ATTEMPTS);
    }
 
     // Unregister this account/device pair within the server.
     void unregister(final Context context, final String regId) {
          
        Log.i(Config.TAG, "unregistering device (regId = " + regId + ")");
         
        String serverUrl = Config.YOUR_SERVER_URL + "/unregister";
        Map<String, String> params = new HashMap<String, String>();
        params.put("regId", regId);
         
        try {
            post(serverUrl, params);
            GCMRegistrar.setRegisteredOnServer(context, false);
        //    String message = context.getString(R.string.server_unregistered);
            controller.displayMessageOnScreen(context, "Unregistered");
        } catch (IOException e) {
             
            // At this point the device is unregistered from GCM, but still
            // registered in the our server.
            // We could try to unregister again, but it is not necessary:
            // if the server tries to send a message to the device, it will get
            // a "NotRegistered" error message and should unregister the device.
             
//            String message = context.getString(R.string.server_unregister_error,
//                    e.getMessage());
        	controller.displayMessageOnScreen(context, "Unregistered server error"+ e.getMessage());
        }
    }
 
    // Issue a POST request to the server.
    private static void post(String endpoint, Map<String, String> params)
            throws IOException {    
         
        URL url;
        try {
             
            url = new URL(endpoint);
             
        } catch (MalformedURLException e) {
            throw new IllegalArgumentException("invalid url: " + endpoint);
        }
         
        // Iterates through params and appends each of the parameters(name, mail, regid) to the URL
        StringBuilder bodyBuilder = new StringBuilder();
        Iterator<Entry<String, String>> iterator = params.entrySet().iterator();
         
        // constructs the POST body using the parameters
        while (iterator.hasNext()) {
            Entry<String, String> param = iterator.next();
            bodyBuilder.append(param.getKey()).append('=')
                    .append(param.getValue());
            if (iterator.hasNext()) {
                bodyBuilder.append('&');
            }
        }
         
        String body = bodyBuilder.toString();
         
        Log.v(Config.TAG, "Posting '" + body + "' to " + url);
         
        byte[] bytes = body.getBytes();
         
        HttpURLConnection conn = null;
        try {
             
            Log.e("URL", "> " + url);
             
            conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            conn.setUseCaches(false);
            conn.setFixedLengthStreamingMode(bytes.length);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type",
                    "application/x-www-form-urlencoded;charset=UTF-8");
            // post the request
            OutputStream out = conn.getOutputStream();
            out.write(bytes);
            out.close();
             
            // handle the response
            int status = conn.getResponseCode();
             
            // If response is not success
            if (status != HttpStatus.SC_OK) {
                 
              throw new IOException("Post failed with error code " + status);
            }
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }
      }
     
     
     
 
  
    
}