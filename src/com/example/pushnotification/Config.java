package com.example.pushnotification;

public interface Config {
	 
    
    // CONSTANTS
	//URL of the server
    static final String YOUR_SERVER_URL =  
                          "http://192.168.10.199:80/APNS/register.php";
     
    // Google project id(obtained from Google API Console)
    static final String GOOGLE_SENDER_ID = "980065837792"; 
 
    /**
     * Tag used on log messages.
     */
    static final String TAG = "GCM Android Example";
 
    static final String DISPLAY_MESSAGE_ACTION =
            "com.androidexample.gcm.DISPLAY_MESSAGE";
 
    static final String EXTRA_MESSAGE = "message";
         
     
}