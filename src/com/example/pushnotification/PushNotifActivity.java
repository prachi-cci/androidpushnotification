package com.example.pushnotification;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gcm.GCMRegistrar;
 
public class PushNotifActivity extends Activity {
    // label to display gcm messages
    TextView lblMessage;
    ServerController aController;
    Controller controller;
    // Asyntask
    AsyncTask<Void, Void, Void> mRegisterTask;
     
    public static String name;
    public static String email;
 
    @Override
    public void onCreate(Bundle savedInstanceState) { 
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_push_notif);
         
        //Get Global ServerController Class object (see application tag in AndroidManifest.xml)
        aController = (ServerController) getApplicationContext();
        controller = (Controller) getApplicationContext();
         
        // Check if Internet present
        if (!controller.isConnectingToInternet()) {
             
            // Internet Connection is not present
        	controller.showAlertDialog(PushNotifActivity.this,
                    "Internet Connection Error",
                    "Please connect to Internet connection", false);
            // stop executing code by return
            return;
        }
         
        // Getting name, email from intent
        Intent i = getIntent();
         
        name = i.getStringExtra("name");
        email = i.getStringExtra("email");      
         
        // Make sure the device has the proper dependencies.
        GCMRegistrar.checkDevice(this);
 
        // Make sure the manifest permissions was properly set 
        GCMRegistrar.checkManifest(this);
 
        lblMessage = (TextView) findViewById(R.id.lblMessage);
         
        // Register custom Broadcast receiver to show messages on activity
        registerReceiver(mHandleMessageReceiver, new IntentFilter(
                Config.DISPLAY_MESSAGE_ACTION));
        // Get GCM registration id
        final String regId = GCMRegistrar.getRegistrationId(this);
 
        // Check if regid already presents
        if (regId.equals("")) {
            // Register with GCM            
           GCMRegistrar.register(this, Config.GOOGLE_SENDER_ID);
        //	GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);
        //	 String registrationId = gcm.register(Config.GOOGLE_SENDER_ID);
             
        } else {
             
            // Device is already registered on GCM Server
            if (GCMRegistrar.isRegisteredOnServer(this)) {
                 
                // Skips registration.              
                Toast.makeText(getApplicationContext(), 
                              "Already registered with GCM Server", 
                              Toast.LENGTH_LONG).
                              show();
             
            } else {
                 
                // Try to register again, but not in the UI thread.
                // It's also necessary to cancel the thread onDestroy(),
                // hence the use of AsyncTask instead of a raw thread.
                 
                final Context context = this;
                mRegisterTask = new AsyncTask<Void, Void, Void>() {
 
                    @Override
                    protected Void doInBackground(Void... params) {
                         
                        // Register on our server
                        // On server creates a new user
                        aController.register(context, name, email, regId);
                         
                        return null;
                    }
 
                    @Override
                    protected void onPostExecute(Void result) {
                        mRegisterTask = null;
                    }
 
                };
                 
                // execute AsyncTask
                mRegisterTask.execute(null, null, null);
            }
        }
    }       
 
    // Create a broadcast receiver to get message and show on screen 
    private final BroadcastReceiver mHandleMessageReceiver = new BroadcastReceiver() {
         
        @Override
        public void onReceive(Context context, Intent intent) {
             
            String newMessage = intent.getExtras().getString(Config.EXTRA_MESSAGE);
             
            // Waking up mobile if it is sleeping
            controller.acquireWakeLock(getApplicationContext());
             
            // Display message on the screen
            lblMessage.append(newMessage + " ");         
             
            Toast.makeText(getApplicationContext(), 
                           "Got Message: " + newMessage, 
                           Toast.LENGTH_LONG).show();
             
            // Releasing wake lock
            controller.releaseWakeLock();
        }
    };
     
    @Override
    protected void onDestroy() {
        // Cancel AsyncTask
        if (mRegisterTask != null) {
            mRegisterTask.cancel(true);
        }
        try {
            // Unregister Broadcast Receiver
            unregisterReceiver(mHandleMessageReceiver);
             
            //Clear internal resources.
            GCMRegistrar.onDestroy(this);
             
        } catch (Exception e) {
            Log.e("UnRegister Receiver Error", 
                      "> " + e.getMessage());
        }
        super.onDestroy();
    }
 
}

